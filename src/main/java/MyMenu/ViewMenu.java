package MyMenu;
import Task.StringUtils;

import java.util.*;

public class ViewMenu {
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    Locale locale;
    ResourceBundle bundle;

    private void setMenu() {

        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("6", bundle.getString("6"));
        menu.put("7", bundle.getString("7"));
        menu.put("8", bundle.getString("8"));
        menu.put("9", bundle.getString("9"));
        menu.put("Q", bundle.getString("Q"));
    }

    public ViewMenu() {
        locale = new Locale("hi");
        locale = new Locale("uk");
        locale = new Locale("zh","CH");
        locale = new Locale("ja");
        locale = new Locale("haw");
        locale = new Locale("vi");
        locale = new Locale("ka");

        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        methodsMenu = new LinkedHashMap<>();

        methodsMenu.put("1", this::testStringUtils);
        methodsMenu.put("2", this::internationalizeMenuUkrainian);
        methodsMenu.put("3", this::internationalizeMenuEnglish);
        methodsMenu.put("4", this::internationalizeMenuChinese);
        methodsMenu.put("5", this::internationalizeMenuHindi);
        methodsMenu.put("6", this::internationalizeMenuJapanese);
        methodsMenu.put("7", this::internationalizeMenuHawaii);
        methodsMenu.put("8", this::internationalizeMenuVietnamese);
        methodsMenu.put("9", this::internationalizeMenuGeorgian);
    }

    private void testStringUtils() {
        StringUtils utils = new StringUtils();
        utils.addToParameters("Wensday")
                .addToParameters(":")
                .addToParameters(14-11-2018);
        System.out.print(utils.concat());
    }

    private void internationalizeMenuUkrainian() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }

    private void internationalizeMenuEnglish() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }

    private void internationalizeMenuChinese() {
        locale = new Locale("zh");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }

    private void internationalizeMenuHindi() {
        locale = new Locale("hi");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }
    private void internationalizeMenuJapanese() {
        locale = new Locale("ja");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }
    private void internationalizeMenuHawaii() {
        locale = new Locale("haw");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }
    private void internationalizeMenuVietnamese() {
        locale = new Locale("vi");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }
    private void internationalizeMenuGeorgian() {
        locale = new Locale("vi");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }


    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String key : menu.keySet()) {
            if (key.length() == 1) {
                System.out.println(menu.get(key));
            }
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

}
